// Bot to cooment on issues created in GitLab project

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
)

// Define gitlab issue webhook json structure
// https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#issues-events
type Message struct {
	Object_kind      string            `json:"object_kind"`
	User             *User             `json:"user"`
	Project          *Project          `json:"project"`
	ObjectAttributes *ObjectAttributes `json:"object_attributes"`
}
type User struct {
	Name     string `json:"name"`
	Username string `json:"username"`
}
type Project struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type ObjectAttributes struct {
	ID    int    `json:"iid"`
	Title string `json:"title"`
}

func Main(rq *Request) (interface{}, *Response) {
	var d Message
	baseurl := "https://gitlab.com/api/v4"
	note := "Thanks%20for%20reporting%20a%20new%20issue!"
	// We store gitlab token in Swifty Accounts - secure storage for sensitive data
	apitoken := os.Getenv("ACC_GENERICGITLABTOKEN_TOKEN")

	err := json.Unmarshal([]byte(rq.Body), &d)
	if err != nil {
		return "error", nil
	}

	// Printing Project and Issue IDs for troubleshooting purpose
	fmt.Printf("Project:" + strconv.Itoa(d.Project.ID))
	fmt.Printf("Object:" + strconv.Itoa(d.ObjectAttributes.ID))

	// Sending request to create new note (comment) for the issue
	// https://docs.gitlab.com/ee/api/notes.html#create-new-issue-note
	url := baseurl + "/projects/" + strconv.Itoa(d.Project.ID) + "/issues/" + strconv.Itoa(d.ObjectAttributes.ID) + "/notes?body=" + note

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Private-Token", apitoken)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("HTTP Error!")
		panic(err)
	}
	defer resp.Body.Close()

	// Printing GitLab response for troubleshooting purpose
	fmt.Printf(resp.Status)

	return resp.Status, nil
}
